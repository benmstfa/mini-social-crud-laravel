var postId = 0;
var postBodyElement = null;

$('.post').find('.interaction').find('.edit').on('click', function (event) {

    event.preventDefault();
    postBodyElement = event.target.parentNode.parentNode.childNodes[1];
    var postbody = postBodyElement.textContent;

    postId = event.target.parentNode.parentNode.dataset['postid'];
    document.getElementById('post_body').innerText = postbody;
});

$('#modal_save').on('click', function () {
    console.log($('#post_body').val());
    $.ajax({
        method: 'post',
        url: urlEdit,
        data: {
            body: $('#post_body').val(),
            postId: postId,
            _token: token
        },
        success: function (data) {
            // console.log(JSON.stringify(data));
            $(postBodyElement).text(data['new_body']);
            $('#editModal').modal('hide');
        }
    });
});

$('.like').on('click', function (event) {
    console.log("trying");
    event.preventDefault();
    var isLike = event.target.previousElementSibling == null;
    postId = event.target.parentNode.parentNode.dataset['postid'];
    $.ajax({
        method: 'post',
        url: urlLike,

        data: {
            isLike: isLike,
            postId: postId,
            _token: token
        },
        success: function (data) {
            console.log(data);
        }
    });

    // console.log(isLike);
});

