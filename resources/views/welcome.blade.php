@extends('layouts.master')

@section('title')
    Welcome!
@endsection

@section('content')

    <h1>Create Post</h1>

    @include('includes.message_block')

    <div class="row">
        <div class="col-md-6">
            <h3>Sign Up</h3>
            <form action="{{ route('signup') }}" method="post" class="" novalidate>
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" name="email"
                           class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email"
                           value="{{ Request::old('email') }}">
                </div>
                <div class="form-group">
                    <label for="first-name">Your First Name</label>
                    <input type="text" name="first_name"
                           class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}" id="first_name"
                           value="{{ Request::old('first_name') }}">
                </div>
                <div class="form-group">
                    <label for="last-name">Your Last Name</label>
                    <input type="text" name="last_name"
                           class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}" id="last_name"
                           value="{{ Request::old('last_name') }}">
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password"
                           class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" id="password"
                           value="{{ Request::old('password') }}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

        <div class="col-md-6">
            <h3>Sign In</h3>
            <form action="{{ route('signin') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" name="email" class="form-control" id="email"
                           value="{{ Request::old('email') }}">
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password"
                           value="{{ Request::old('password') }}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection