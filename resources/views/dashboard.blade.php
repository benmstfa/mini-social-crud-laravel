@extends('layouts.master')

@section('title')
    Dashboard
@endsection

@section('content')
    @include('includes.message_block')
    <section class="row new-post justify-content-center ">
        <div class="col-md-6 col-md-offset-3">
            <header>
                <h3>What do you have to say</h3></header>

            <form action="{{ route('post.create') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <textarea class="form-control" name="body" id="ne-pot" cols="5" rows="10"
                              placeholder="Your Post"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Create Post</button>
            </form>
        </div>
    </section>
    <section class="row posts">
        <div class="col-md-6 col-md-3-offset">

            <header>
                <h3>What other People say ...</h3>
            </header>

            @foreach($posts as $post)

                <article class="post" data-postid = "{{ $post->id }}">
                    <p>{{ $post->body }}</p>
                    <div class="info">
                        Posted By {{ $post->user->first_name}} on {{ $post->created_at }}
                    </div>
                    <div class="interaction">
                        <a href="#" class="like">Like</a> |
                        <a href="#" class="like">Dislike</a>
                        @if(Auth::user() == $post->user)
                            |
                            <a href="#" class="edit" data-toggle="modal" data-target="#editModal">Edit</a> |
                            <a href="{{ route('post.delete', ['post_id' => $post->id]) }}">Delete</a>
                        @endif
                    </div>
                </article>
            @endforeach
        </div>
    </section>

    <div class="modal fade" id="editModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Post</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="form-group">
                            <label for="post_body">Edit the Post</label>
                            <textarea class="form-control" name="post_body" id="post_body"  ></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="modal_save">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <script>

        var token = '{{ Session::token() }}';
        var urlEdit = '{{ route('edit') }}';
        var urlLike = '{{ route('like') }}';

    </script>
@endsection
