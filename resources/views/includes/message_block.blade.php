@if ($errors->any())
    <div class="alert alert-danger error">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('message'))
    <div class="row">
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    </div>
@endif