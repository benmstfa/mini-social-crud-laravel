<!doctype html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<body>
<header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #0069D9">
        <a class="navbar-brand" href="{{ route('dashboard') }}">Dashboard</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="{{ route('account') }}" class="nav-link">Account</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link">Logout</a>
                </li>
            </ul>
        </div>
    </nav>

</header>
</body>
</html>