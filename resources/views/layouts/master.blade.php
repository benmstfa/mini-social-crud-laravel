<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="{{ URL::to('/css/custom.css') }}">

</head>
<body>
@include('includes.header')

<div class="container">
    @yield('content')
</div>
<script src="/js/app.js"></script>
<script src="/js/custom.js"></script>
</body>
</html>